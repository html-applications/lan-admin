// Выделяет и снимает выделение с группы чекбоксов (с зажатым Shift)
function CheckHosts() {
	if (event.shiftKey) {
		var _check = false;
		var ChkBox = event.srcElement;
		var idxChkBox = GetElementIndex(idMarkHost, ChkBox);
		var aMarkHost = document.getElementsByName("idMarkHost");
		for (var i=0; i<aMarkHost.length; i++) {
			if ( (aMarkHost[i].checked==ChkBox.checked) || (i == idxChkBox) ) {
				_check = !_check;
			}
			if (_check) aMarkHost[i].checked = ChkBox.checked;
		}
	}
	ShowHostsCount();
}

// Отмечает хосты в списке (по-умолчнию - только online, c Shift - все подряд, с Ctrl - только offline)
function CheckAll(){
	var arrMarkHost = document.getElementsByName('idMarkHost');
	for (var i=0; i<arrMarkHost.length; i++) {
		if (idCheckAll.checked){
			if (event.shiftKey) {
				arrMarkHost[i].checked = true;
			} else if (event.ctrlKey) {
				if (aHosts[i].online < 0) arrMarkHost[i].checked = true;
			} else {
				if (aHosts[i].online >= 0) arrMarkHost[i].checked = true;
			}
		} else {
			arrMarkHost[i].checked = false;
		}
	}
	ShowHostsCount();
}

// Снимает отметку со всех хостов
function UncheckAll(){
	var aMarkHost = document.getElementsByName("idMarkHost");
	for (var i=0; i<aMarkHost.length; i++) aMarkHost[i].checked = false;
	idCheckAll.checked = false;
	ShowHostsCount();
}

// Создает заголовок таблицы
function CreateHeaders() {
	var align, input_htm;
	for (var j = 0; j < aColumnsHeader.length; j++) {
		with (idHeaderFixed.insertCell()) {
			align = (j==0) ? 'left' : '';
			input_htm = (j == 0) ? '&nbsp;<input id="idCheckAll" type=checkbox class=chk hidefocus onclick="CheckAll()">&nbsp;' : '';
			innerHTML = '<td><table class="tHeader"><tr><th align="' + align + '" onmouseup="if (event.button==2) {oCM.column=' + j + '; ShowContextMenuHeader();}">' + input_htm + aColumnsHeader[j] + '</th></tr></table></td>';
		}
		with (idHeaderFixed.insertCell()) {
			id = "idResizer";
			onmousedown = ColumnResize_start;
		}
	}
	// Восстановление сохраненных размеров колонок
	if (aColumnsWidth) {
		var width;
		for (var i = 0; i < idHeaderFixed.cells.length; i+=2) {
			width = aColumnsWidth[i/2];
			if (width) idHeaderFixed.cells[i].style.width = width + 'px';
		}
	}
}

// Добавляет ряд в таблицу
function AddRow() {
	var HostRow = idHostsList.insertRow();
	HostRow.onmouseover = function(){ // Подсветка текущего ряда
		if (!(oCM.visible || drag_row)) this.className = "hglight";
	};
	HostRow.onmouseout  = function(){ // Снимаем подсветку с текущего ряда
		if (!(oCM.visible || drag_row)) this.className = "";
	};
	HostRow.onmouseup = function() { // Вызов контекстного меню
		if (event.button==2) {
			copy_text = event.srcElement.innerText;
			oCM.row = this.rowIndex-1;
			oCMSet ? ShowContextMenuRow() : ShowContextMenuDefault();
		}
	};
	HostRow.ondblclick = function() { // Действие по умолчанию
		oCM.row = this.rowIndex-1;
		var item = oCMSet['cm_default'];
		if (item && oCMSet['cm_cmd'+item]) {
			ContextMenuRunItem(oCMSet['cm_cmd'+item]);
		} else {
			if (oWshShell.Popup("Default command is not defined!\n\nEdit context menu now?", 5, app_name, 52) == 6){
				EditContextMenu();
			}
		}
	};

	// Перетаскивание ряда
	HostRow.onmousedown = rowOnMouseDown;
	HostRow.onmousemove = rowOnMouseMove;

	// Добавляем ячейки
	for (var j=0; j<aColumnsContent.length; j++) {
		with (HostRow.insertCell()) {
			className = 'tdata';
			colSpan = 2;
		}
	}
}

// Заполняет ряд таблицы данными
function FillRow(i){
	// Замещает все найденные в тексте <параметры> соответсвующими их значениями (i - номер хоста)
	function ExtractValue (param, i) {
		function replacer(_sub, S1, S2, _offset, _str) {
			return ((S1=='') ? oMainSet : (IsProtect(i, S2) ? ('<font color=gray>'+aHosts[i][S2]+'<font>') : aHosts[i][S2])) || '';
		}
		var re = /<([^.]*?)[.]?([^.>]*?)>/g;
		var value = param.replace(re, replacer);
		var empty_value = param.replace(re, '');
		return (value!=empty_value) ? value : '&nbsp;';
	}
	// Преобразует строку '[11|22|33]' в '<select><option>11</option><option>22</option><option>33</option></select>'
	function CreateSelect(str){
		var aOptions = str.split('|');
		var strSelect = '<select style="width:100%" title="' + str + '">';
		for(var n = 0, option; option = aOptions[n++];) {
			strSelect += '<option>' + option + '</option>\n';
		}
		strSelect += '</select>';
		return strSelect;
	}

	var host = aHosts[i];
	var oRow = idHostsList.rows[i];
	var html, oCell, contents;
	for (var j=0; j<aColumnsContent.length; j++) {
		oCell = oRow.cells[j];
		oCell.style.whiteSpace = "nowrap";
		html = (j==0) ? '<input id="idMarkHost" type=checkbox class=chk hidefocus onclick="CheckHosts()"><img id="idIcon" src="images/' + (host.type || 'computer') + '.ico">&nbsp;' : '';
		if (!/<host\.online>/.test(aColumnsContent[j])) {
			contents = ExtractValue(aColumnsContent[j], i);
			if (/^\[(.+\|.+)\]$/.test(contents)) { // для значений вида [a|b|c]
				contents = RegExp.$1;
				oCell.innerHTML = html + '<span id="idContents">' + CreateSelect(contents) + '</span>';
				oCell.title = contents.replace(/\|/g, '\r\n');
			} else {
				oCell.innerHTML = html + '<span id="idContents">' + contents + '</span>';
				if (/\S+/.test(oCell.innerText)) oCell.title = oCell.innerText;
			}
		} else {
			oCell.innerHTML = '<span id="idOnline">n</span>'
			oCell.style.textAlign = 'center';
		}
	}
	setTimeout('RowEnabled('+i+')', 0);
}

// Установка цвета ячейки idOnline в зависимости от значения ResponseTime
function ResponseTimeColor(resp_time){
	var p = Math.floor((256*resp_time/Number(oMainSet.response_time_max)));
	if (p > 255) return 'rgb(255,0,0)';
	var r = (p<128) ? p*2 : 255;
	var g = (p<=128) ? 255 : 512-p*2;
	return 'rgb('+r+ ',' + g + ',0)';
}

// Делает (не)активным ряд в зависимости от значения host.online
function RowEnabled(i){
	var host = aHosts[i];
	var oRow = idHostsList.rows[i];
	var aContents = oRow.getElementsByTagName('SPAN');
	var oIcon = document.getElementsByName('idIcon')[i];
	var oOnline = document.getElementsByName('idOnline')[i];
	if (oOnline && (host.online >= 0)) {
		oOnline.style.color = ResponseTimeColor(host.online);
		oOnline.title = host.online + 'ms';
	}
	if (host.online >= 0) { // online
		oIcon.style.filter = '';
		if (aContents[0].disabled) { // только если строка задизаблена, делаем ее активной
			for (var i = 0, content; content = aContents[i++];) content.removeAttribute("disabled");
		}
	} else { // offline
		if (!aContents[0].disabled) { // только если строка активна, дизаблим ее
			oIcon.style.filter = 'Xray';
			for (var i = 0, content; content = aContents[i++];) content.disabled = true;
		}
	}
}

// Добавление новой колонки
function ColumnAdd(){
	column_add = true;
	idHeaderFixed.cells[oCM.column*2].style.backgroundColor = "infobackground";
	showModalDialog('code/column.html', self, 'status:no; help:no; dialogWidth:490px; dialogHeight:30px;');
	idHeaderFixed.cells[oCM.column*2].style.backgroundColor = "";
}

// Редактирование отображаемых в колонке данных
function ColumnEdit(){
	column_add = false;
	idHeaderFixed.cells[oCM.column*2].style.backgroundColor = "infobackground";
	var oMainSet_tmp = ObjClone(oMainSet);
	showModalDialog('code/column.html', self, 'status:no; help:no; dialogWidth:490px; dialogHeight:30px;');
	ObjCompare(oMainSet, oMainSet_tmp, oMainSet);
	idHeaderFixed.cells[oCM.column*2].style.backgroundColor = "";
}

// Удаление выбранной колонки
function ColumnDelete(){
	aColumnsHeader.splice(oCM.column, 1);
	oMainSet.column_headers = aColumnsHeader.join(',');
	aColumnsContent.splice(oCM.column, 1);
	oMainSet.column_contents = aColumnsContent.join(',');
	aColumnsWidth.splice(oCM.column+1, 1);
	oMainSet.column_width = aColumnsWidth.join(',');
	Cookie('column_width', oMainSet.column_width);
	ShowHostsTable();
	SaveButtonEnable(oMainSet);
}

// Сортировка таблицы по выбранной колонке
function ColumnSort(){
	// сравнение по алфавиту
	function compareAlph(a, b) {
		var x = Html2text(a[ct]);
		var y = Html2text(b[ct]);
		if (x < y) return -1;
		if (x > y) return 1;
		return 0;
	}

	// сравнение по числовому значению (точнее - по сумме)
	function compareNum(a, b) {
		function StrSum(str){
			var sum = 0;
			var re = /(\d+)/g;
			while (re.exec(str)){
				sum += Number(RegExp.lastParen);
			}
			return sum;
		}
		var x = a[ct] ? StrSum(a[ct]) : 0;
		var y = b[ct] ? StrSum(b[ct]) : 0;
		return (x - y);
	}

	// сравнение по IP
	function compareIP(a, b) {
		function ip2num(ip) {
			var arr = /(\d+)\.(\d+)\.(\d+)\.(\d+)/.exec(ip);
			return arr ? (arr[1]*16777216 + arr[2]*65536 + arr[3]*256 + Number(arr[4])) : 0;
		}
		return ip2num(a[ct]) - ip2num(b[ct]);
	}

	// сравнение по дате (в формате dd.mm.yyyy)
	function compareDate(a, b) {
		function date2str(date) {
			var arr = /(\d+)\.(\d+)\.(\d+)/.exec(date);
			return arr ? Number('' + arr[3] + arr[2] + arr[1]) : 0;
		}
		return date2str(a[ct]) - date2str(b[ct]);
	}

	// определение типа содержимого ячеек выбранной колонки
	function GetDataType() {
		var i = aHosts.length; while (i--) {
			val = aHosts[i][ct];
			if (val) {
				if (/^\d+\.\d+\.\d+\.\d+$/.test(val)) return "ip";
				if (/^\d\d\.\d\d\.\d\d+$/.test(val)) return "date";
				if (/^\d+$/.test(val)) return "num";
				return;
			}
		}
	}

	var ct = aColumnsContent[oCM.column].match(/<host\.(.*?)>/)[1];

	switch(GetDataType()){
		case "ip":
			aHosts.sort(compareIP); break;
		case "num":
			aHosts.sort(compareNum); break;
		case "date":
			aHosts.sort(compareDate); break;
		default:
			aHosts.sort(compareAlph);
	}
	ShowHostsTable();
	SaveButtonEnable(aHosts);
}

var dW = dH = 0; // разница между размером окна и размером клиентской области
// Обработка эвента изменения размеров окна
function Window_OnResize(){
	if (window.screenLeft!=0) { // если не нажали кнопку "Развернуть"
		oMainSet.width = document.body.clientWidth + dW;
		oMainSet.height = document.body.clientHeight + dH;
		Cookie('win_size', oMainSet.width + ',' + oMainSet.height);
	}
}

// Фиксация заголовка таблицы при прокрутке
function HeaderFix(){
	idHeaderFixed.style.top=idTableScroll.scrollTop-2+'px';
}

function ShowHostsCount(){
	var selected_hosts_count = 0;
	var arrMarkHost = document.getElementsByName('idMarkHost');
	for (var i=0; i<arrMarkHost.length; i++) if (arrMarkHost[i].checked) selected_hosts_count++;
	idHostsCount.innerText = aHosts.length + '/' + selected_hosts_count;
}

function ShowHostsTable(){
	RemoveChildren(idHeaderFixed);
	RemoveChildren(idHostsList);
	for (var i=0; i<aHosts.length; i++) {
		AddRow();
		FillRow(i);
	}
	CreateHeaders();
	ShowHostsCount();
	StartTimer();
}

// Действия при открытии окна приложения
function Window_OnLoad() {
	dW = oMainSet.width - document.body.clientWidth;
	dH = oMainSet.height - document.body.clientHeight;
	ShowHostsTable();
	idFindText.focus();
}

// Удаление обработки события перемещения мыши
function DeleteEvents(){
	var Rows = idHostsList.rows;
	for (var i=0; i<Rows.length; i++) {
		Rows[i].onmouseover = null;
		Rows[i].onmouseout = null;
	}
}

// Возвращает ProcessId нашего приложения
function GetMyPID(){
	var pid = new ActiveXObject('WScript.Shell').Exec('rundll32 kernel32,Sleep').ProcessID;
	var oProc = new Enumerator(GetObject('winmgmts:\\\\.\\root\\cimv2').ExecQuery('select * from Win32_Process where ProcessId='+pid)).item();
	var parent_id = oProc.ParentProcessId;
	oProc.Terminate();
	return parent_id;
}

var myPID = GetMyPID();  // ProcessId "LAN Administrator"-а

// Действия перед закрытием окна приложения
function Window_OnBeforeUnLoad() {
	StopTimer();
	DeleteEvents();
	if ((!idSave.disabled) && (oWshShell.Popup("Save current settings?", 0, document.title, 292)==6)) {
		SaveSettings();
	}
	if (!isReload) oWshShell.Run('taskkill /f /pid '+myPID+' /t', 0);
}

// Обработка нажатий на клавиши
function OnKeyDown(){
	switch(event.keyCode){
		case 112: // F1
			open("readme.html", "", "menubar=no,scrollbars=yes,status=no");
			break;
		case 27: // Esc
			if (idFindText.value) { // Очистка результатов поиска текста
				idFindText.value = '';
				ClearFounds();
			} else {                // Завершение работы
				self.close();
			}
			break;
		case 38: // Up (на предыдущее вхождение текста)
			if (Number(idFoundCount.innerText) > 1) MarkCurrentFound(-1);
			break;
		case 40: // Down (на следующее вхождение текста)
			if (Number(idFoundCount.innerText) > 1) MarkCurrentFound(1);
			break;
	}
}
