// Копирование в буфер обмена
var copy_text = ''; // глобальная переменная, получающая текст ячейки при открытии на ней контекстного меню
function CopyToClipboard(){
	clipboardData.setData("Text", copy_text);
	HHCtrl.TextPopup(copy_text, "MS Sans Serif, 8, , plain", 4, 4, -1, -1);
}

// HTTP подключение к удаленному хосту
function HTTPConnect(i) {
	var protocol;
	switch(aHosts[i].port_check){
		case '21':
			protocol = 'ftp://';
			break;
		case '443':
			protocol = 'https://';
			break;
		default:
			protocol = 'http://';
	}
	window.open (protocol + NameOrIp(i), '_blank');
}

// Возвращает массив из номеров или имен выбранных хостов
// если нет отмеченных хостов, то возвращает текущий
function GetSelectedHosts(names){
	var SelectedHosts = [];
	for (var i=0; i<aHosts.length; i++) {
		if (document.getElementsByName('idMarkHost')[i].checked) {
			if (names) {
				SelectedHosts[SelectedHosts.length] = NameOrIp(i);
			} else {
				SelectedHosts[SelectedHosts.length] = i;
			}
		}
	}
	if (SelectedHosts.length==0) {
		if (names) {
			SelectedHosts[0] = NameOrIp(oCM.row);
		} else {
			SelectedHosts[0] = oCM.row;
		}
	}
	return SelectedHosts;
}

// Запрос инфы о выбранных хостах
function GetSelHostInfo(){
	var sel_hosts = GetSelectedHosts();
	if (!sel_hosts) return;
	StopTimer();
	idStatus.innerText = 'Get info from selected hosts...';
	for (var i=0; i<sel_hosts.length; i++) {
		pending++;
		setTimeout('GetHostInfo('+sel_hosts[i]+')', 10);
	}
}

// Удаление выбранных хостов
function RemoveSelHost(){
	var sel_hosts = GetSelectedHosts(true);
	if (!sel_hosts) return;
	if (oWshShell.Popup('Are you sure you want to delete the host:\n' + sel_hosts.join(','), 0, app_name, 36) == 6) {
		StopTimer();
		sel_hosts = GetSelectedHosts();
		for (var i=sel_hosts.length-1; i>=0; i--) {
			idHostsList.deleteRow(sel_hosts[i]);
			aHosts.splice(sel_hosts[i], 1);
		}
		ShowHostsCount();
		SaveButtonEnable(aHosts);
		StartTimer();
	}
}

var sHosts = ""; // Строка со списком выбранных хостов (через запятую)
// Запуск команды на удаленных ПК
function ExecRemote(){
	var sel_hosts = GetSelectedHosts(true);
	if (sel_hosts) {
		sHosts = sel_hosts.join(',');
		sHosts = sHosts.replace(/\.[a-z.]+\b/g, '');
		StopTimer();
		showModalDialog('code/exec.remote.html', self, 'status:no; help:no; dialogWidth:10px; dialogHeight:30px;');
		StartTimer();
		UncheckAll();
	}
}

// Экспорт инфы в Excel
function ExportToExcel(){
	var sel_hosts = GetSelectedHosts();
	if (!sel_hosts) return;

	StopTimer();
	var objXL = new ActiveXObject("Excel.Application");
	objXL.Visible = true;
	objXL.WorkBooks.Add;

	var column = [];
	function GetColumn(name){
		for (var col=1; col<column.length; col++) {
			if (column[col] == name) return col; // exist header
		}
		// new header
		column[col] = name;

		var cell = objXL.Cells(1, col);
		cell.Value = name;
		cell.HorizontalAlignment = -4108; //xlCenter
		cell.Font.Bold = true;

		return col;
	}

	var row = 2;
	for (var i=0; i<sel_hosts.length; i++) {
		var host = aHosts[sel_hosts[i]];
		for (var j in host){
			with (objXL.Cells(row, GetColumn(j))) {
				NumberFormat = "@";
				Value = host[j];
			}
		}
		row++;
	}
	objXL.Cells.Columns.AutoFit;
	StartTimer();
	UncheckAll();
}

// Импорт инфы из Excel
function ImportFromExcel(){
	var objXL = new ActiveXObject("Excel.Application");
	objXL.Visible = true;
	var filename = objXL.GetOpenFilename("Excel files (*.xls*), *.xls*");
	if ((filename) && (oWshShell.Popup("Warning:\nAll your data will be replaced by data from Excel !", 5, "Import from Excel", 4388) == 6)){
		aHosts = [];
		objXL.Visible = false;
		objXL.Workbooks.Open(filename);
		var host, name, val;
		for (var row = 2; row <= objXL.ActiveSheet.UsedRange.Rows.Count; row++) {
			host = {};
			for (var col = 1; col <= objXL.ActiveSheet.UsedRange.Columns.Count; col++) {
				name = objXL.Cells(1, col).Text;
				val = objXL.Cells(row, col).Text;
				if (val) host[name] = val;
			}
			aHosts.push(host);
		}
		objXL.Workbooks.Close();
		ShowHostsTable();
		SaveButtonEnable(aHosts);
	}
}

function EditContextMenu(){
	StopTimer();
	var oCMSet_tmp = ObjClone(oCMSet);
	showModalDialog('code/edit.context.menu.html', self, 'status:no; help:no; resizable:yes; dialogWidth:600px; dialogHeight:500px;');
	ObjCompare(oCMSet, oCMSet_tmp, oCMSet);
	StartTimer();
}

// Редактирование инфы о хосте (вызов диалога)
function EditHostInfo(){
	StopTimer();
	idHostsList.rows[oCM.row].style.backgroundColor = "infobackground";
	var aHosts_tmp = ObjClone(aHosts[oCM.row]);
	showModalDialog('code/edit.host.info.html', self, 'status:no; help:no; resizable:yes; dialogWidth:450px; dialogHeight:500px;');
	ObjCompare(aHosts[oCM.row], aHosts_tmp, aHosts);
	idHostsList.rows[oCM.row].style.backgroundColor = "";
	StartTimer();
}

// Добавление новых хостов (вызов диалога)
function AddHosts(){
	StopTimer();
	showModalDialog('code/add.hosts.html', self, 'status:no; help:no; scroll:no; center:yes; dialogWidth:10px; dialogHeight:30px;');
	ShowHostsTable();
}

// Добавление нового хоста в список (вызывается из диалога add.hosts.html)
function AddScanResult(host_id, host_online, host_type, host_descript, host_port){
	var tmp = {};
	tmp = {
		type: host_type,
		description: host_descript,
		online: host_online,
		port_check: host_port
	};
	host_id = String(host_id);
	if (host_id.indexOf(' ') == 0) {
		tmp.name = host_id.substr(1);
	} else {
		tmp.ip = host_id;
	}
	aHosts.push(tmp);
	HostResolve(aHosts.length-1);
	AddRow();
	FillRow(aHosts.length-1);
	SaveButtonEnable(aHosts);
}

// Показ окна с фотографией пользователя
function ShowDialog(url) {
	showModelessDialog(url, self, 'status:no; help:no; dialogWidth:10px; dialogHeight:30px;');
}