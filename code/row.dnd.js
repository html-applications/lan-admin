var drag_row = null; // объект, хранящий перетаскиваемый ряд
var start_idx, prev_idx;
var HL; // Признак того где перетаскивается ряд: 0 - в форме редактирования контекстного меню; 1 - в главном окне
var dragStartId = null; // идентификатор dragStart, запускаемого через setTimeout

function rowOnMouseDown(){
	if (event.button == 1) {
		drag_row = this;
		dragStartId = setTimeout(dragStart, 500);
	}
}

function dragStart(){
	if (drag_row) {
		HL = (drag_row.parentNode.id == 'idHostsList') ? 1 : 0;
		start_idx = drag_row.rowIndex-HL;
		prev_idx = start_idx;
		drag_row.className = 'dragRow';
	}
}

function rowOnMouseMove(){
	function getRowEvent(){
		var elem = event.srcElement;
		while (elem.tagName != 'TABLE') {
			if (elem.tagName == 'TR') return elem;
			elem = elem.parentNode;
		}
	}

	if (drag_row && drag_row.className == 'dragRow') {
		var row = getRowEvent();
		if (row) {
			var next_idx = row.rowIndex-HL;
			var table = row.parentNode;
			if (next_idx > prev_idx) { // down
				table.insertBefore(table.rows[drag_row.rowIndex+1-HL], drag_row);
			} else if (next_idx < prev_idx) { // up
				table.insertBefore(drag_row, table.rows[drag_row.rowIndex-1-HL]);
			}
			prev_idx = next_idx;
		}
	}
}

function dragFinish(){
	clearTimeout(dragStartId);
	if (drag_row) {
		var finish_idx = drag_row.rowIndex-HL;
		if (HL && (start_idx != finish_idx)) {
			var tmp = aHosts[start_idx];
			if (finish_idx > start_idx) { // down
				for (var i=start_idx; i<finish_idx; i++) {
					aHosts[i] = aHosts[i+1];
				}
			} else { // up
				for (var i=start_idx; i>finish_idx; i--) {
					aHosts[i] = aHosts[i-1];
				}
			}
			aHosts[finish_idx] = tmp;
			SaveButtonEnable(aHosts);
			ismove = false;
		}
		drag_row.className = '';
		drag_row = null;
	}
}

document.attachEvent('onmouseup', dragFinish);
