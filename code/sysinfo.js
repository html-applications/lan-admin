var oWMIService;
var oWMIRegistry;

// Удаление лишней информации
function ReplaceAdv(str){
	return str.replace(/\(R\)/g, '').replace(/\(TM\)/gi, '').replace(/\(Microsoft Corporation\)/,'').replace(/Microsoft /g,'').replace(/Service Pack /,'SP').replace(/Edition /g,'').replace(/Professional/,'Pro').replace(/CPU /, '').replace(/Процессор|Processor/, '').replace(/(Express|Chipset|Controller|Graphics|Family|Model)/g, '').replace(/\s+/g, ' ').replace(/\s+$/, '').replace(/^\s+/, '');
}

// Return string
function GetPCSerialNumber(){
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_SystemEnclosure");
		var enumItems = new Enumerator(colItems);
		return enumItems.item(0).SerialNumber.trim();
	} catch(e) {return ''};
}

// Retun object {Name, CurrentClockSpeed}
function GetProcessorInfo(){
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_Processor");
		var enumItems = new Enumerator(colItems);
		return {
			Name:              ReplaceAdv(enumItems.item(0).Name),
			CurrentClockSpeed: enumItems.item(0).CurrentClockSpeed
		}
	} catch(e) {return {}};
}

// Return array
function GetDisksSize(){
	var arr_disks_size = [];
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_DiskDrive where MediaType like 'Fixed%'");
		var enumItems = new Enumerator(colItems);
		for (; !enumItems.atEnd(); enumItems.moveNext()) {
			var objItem = enumItems.item();
			var status = objItem.Status;
			if (status != 'OK') oWshShell.Popup(objItem.Caption + " S.M.A.R.T. status: " + status, 5, NameOrIp(i), 16);
			arr_disks_size[arr_disks_size.length] = Math.round(objItem.Size/1000/1000/1000);
		}
	} catch(e) {};
	return arr_disks_size;
}

// Return object {MemorySize, MemoryInfo}
function GetMemoryInfo(i){
	var r = {};
	var aFormFactor = ['Unknown','Other','SIP','DIP','ZIP','SOJ','Proprietary','SIMM','DIMM','TSOP','PGA','RIMM','SODIMM','SRIMM','SMD','SSMP','QFP','TQFP','SOIC','LCC','PLCC','BGA','FPBGA','LGA'];
	var aMemoryType = ['Unknown','Other','DRAM','Synchronous DRAM','Cache DRAM','EDO','EDRAM','VRAM','SRAM','RAM','ROM','Flash','EEPROM','FEPROM','EPROM','CDRAM','3DRAM','SDRAM','SGRAM','RDRAM','DDR','DDR-2'];
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_PhysicalMemory");
		var enumItems = new Enumerator(colItems);
		var arr_mem_size = [];
		var mem_speed = mem_type = mem_formfactor = '';
		for (; !enumItems.atEnd(); enumItems.moveNext()) {
			var objItem = enumItems.item();
			arr_mem_size[arr_mem_size.length] = objItem.Capacity/1024/1024;
			if (objItem.FormFactor) mem_formfactor = aFormFactor[objItem.FormFactor];
			if (objItem.MemoryType) mem_type = aMemoryType[objItem.MemoryType];
			if (objItem.Speed) mem_speed = objItem.Speed;
		}
		r.MemorySize = arr_mem_size.join("+");
		var mem_info = mem_formfactor ? mem_formfactor : '';
		mem_info += mem_type ? ' ' + mem_type : '';
		mem_info += mem_speed ? ' ' + mem_speed : '';
		r.MemoryInfo = mem_info;
	} catch(e) {};
	return r;
}

// Return array
function GetVideoControllers(){
	var r = [];
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_VideoController");
		var enumItems = new Enumerator(colItems);
		for (; !enumItems.atEnd(); enumItems.moveNext()) {
			var objItem = enumItems.item();
			if (/^PCI\\/.test(objItem.PNPDeviceID)) {
				r.push(ReplaceAdv(objItem.Name) + ' (' + Math.round(objItem.AdapterRAM/1024/1024) + ' Mb)');
			}
		}
	} catch(e) {};
	return r;
}

// Return object {Names[], SerialNumbers[]}
function GetMonitors(){
	function GetPNPDeviceID(){
		var ret = [];
		try {
			var colItems = oWMIService.ExecQuery ("SELECT * FROM Win32_PnPEntity WHERE Service='monitor'");
			var enumItems = new Enumerator(colItems);
			for (; !enumItems.atEnd(); enumItems.moveNext()) {
				var objItem = enumItems.item();
				if (objItem.DeviceID) ret.push(objItem.DeviceID);
			}
		} catch(e) {};
		return ret;
	}

	function DecodeEDIDInfo(EDID, is_model){
		var num = 0;
		for (var j = 0; j < EDID.length-4; j++) {
			if (is_model) { // Model name
				if (EDID[j] == 0 && EDID[j + 1] == 0 && EDID[j + 2] == 252 && EDID[j + 3] == 0) num = j + 3;
			} else {        // Serial number
				if (EDID[j] == 0 && EDID[j + 1] == 0 && EDID[j + 2] == 0   && EDID[j + 3] == 255 && EDID[j + 4] == 0) num = j + 4;
			}
		}
		if (num != 0) {
			var ret_string = "";
			var j = 1;
			while (j < 14 && EDID[num + j] != 10){
				ret_string += String.fromCharCode(EDID[num + j]);
				j++;
			}
			return ret_string;
		}
	}

	function DecodeSerialLenovo(EDID){
		var sn = "VNA";
		for (var j = 12; j < 16; j++) sn += String.fromCharCode(EDID[j]);
		return sn;
	}

	var aPNPDeviceID = GetPNPDeviceID();
	var aMonitorName = [];
	var aMonitorSerialNumber = [];
	var monitorName, monitorSerialNumber;
	for (var n = 0; n < aPNPDeviceID.length; n++) {
		var EDID = GetRegistry(oWMIRegistry, 'GetBinaryValue', 'HKLM', 'SYSTEM\\CurrentControlSet\\Enum\\' + aPNPDeviceID[n] + '\\Device Parameters', 'EDID');
		if (EDID) {
			if (EDID[0] == 0 && EDID[1] == 255 && EDID[2] == 255 && EDID[3] == 255 && EDID[4] == 255 && EDID[5] == 255 && EDID[6] == 255 && EDID[7] == 0) {
				monitorName = DecodeEDIDInfo(EDID, true);
				if (monitorName) aMonitorName.push(monitorName);
				monitorSerialNumber = DecodeEDIDInfo(EDID);
				if (!monitorSerialNumber && (/LT2452p/.test(monitorName))) monitorSerialNumber = DecodeSerialLenovo(EDID); // https://support.lenovo.com/ru/ru/documents/ht075949
				if (monitorSerialNumber) aMonitorSerialNumber.push(monitorSerialNumber);
			}
		}
	}
	return {
		Names:         aMonitorName,
		SerialNumbers: aMonitorSerialNumber
	};
}

// Return object {MACAddress, IPAddress[], IPSubnet[], DefaultIPGateway[], DNSServerSearchOrder[]}
function GetNetInfo(){
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_NetworkAdapterConfiguration where IPEnabled = True");
		var enumItems = new Enumerator(colItems);
		var objItem = enumItems.item();
		return {
			MACAddress:           objItem.MACAddress,
			IPAddress:            objItem.IPAddress.toArray(),
			IPSubnet:             objItem.IPSubnet.toArray(),
			DefaultIPGateway:     objItem.DefaultIPGateway.toArray(),
			DNSServerSearchOrder: objItem.DNSServerSearchOrder.toArray()
		};
	} catch(e) {return {}};
}

// Return object {name, domain, username, user, SID}
function GetComputerSystem(){
	var r = {};
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_ComputerSystem");
		var enumItems = new Enumerator(colItems);
		var objItem = enumItems.item();
		r.name = objItem.Name; // host name
		r.domain = objItem.Domain; // dep.fabrikam.com
		if (objItem.UserName) {
			r.username = objItem.UserName; // DOMAIN\LOGIN
			var du = objItem.UserName.split('\\');
			r.user = du[1];
			if (r.name.toLowerCase() == du[0].toLowerCase()) { // local user
				r.SID = oWMIService.Get("Win32_UserAccount.Domain='" + du[0] + "'" + ",Name='" + du[1] + "'").SID;
			} else { // domain user
				r.SID = GetObject("winmgmts:\\\\.\\Root\\CIMV2").Get("Win32_UserAccount.Domain='" + du[0] + "'" + ",Name='" + du[1] + "'").SID;
			}
			if (r.SID) {
				var UserProfile = GetRegistry(oWMIRegistry, 'GetStringValue', 'HKLM', 'SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\ProfileList\\' + r.SID, 'ProfileImagePath');
				if (UserProfile) {
					r.UserProfile = '\\\\' + r.name + '\\' + UserProfile.replace(/^(\w):/, '$1$');
				}
			}
		}
	} catch(e) {};
	return r;
}

// Return array
function GetPrinters(SID){
	var ret = [];
	var defPrinter = GetRegistry(oWMIRegistry, 'GetStringValue', 'HKU', SID + '\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows', 'Device');
	if (defPrinter) {
		defPrinter = defPrinter.split(',')[0];
		ret.push(defPrinter);
	}
	var aPrinters = GetRegistry(oWMIRegistry, 'EnumValues', 'HKU', SID + '\\Software\\Microsoft\\Windows NT\\CurrentVersion\\PrinterPorts');
	if (aPrinters) {
		for (var j = 0; j < aPrinters.length; j++) {
			if (aPrinters[j] != defPrinter) {
				ret.push(aPrinters[j]);
			}
		}
	}
	return ret;
}

// Return array
function GetMappedDrives(SID){
	var ret = [];
	var aDrives = GetRegistry(oWMIRegistry, 'EnumKey', 'HKU', SID + '\\Network');
	if (aDrives) {
		var letter, path;
		for (var j = 0; j < aDrives.length; j++) {
			letter = aDrives[j];
			path = GetRegistry(oWMIRegistry, "GetStringValue", "HKU", SID + '\\Network\\' + letter, 'RemotePath');
			ret.push(letter + '=' + path);
		}
	}
	return ret;
}

// Return string
function GetOperatingSystem(){
	try {
		var colItems = oWMIService.ExecQuery ("select * from Win32_OperatingSystem");
		var enumItems = new Enumerator(colItems);
		for (; !enumItems.atEnd(); enumItems.moveNext()) {
			var objItem = enumItems.item();
			var r = objItem.Caption + ' ' + objItem.Version;
			if (objItem.CSDVersion) r += ' ' + objItem.CSDVersion;
			return ReplaceAdv(r);
		}
	} catch(e) {};
}

// Return object {cn, info, title, mail, telephoneNumber, otherTelephone, wWWHomePage, ...}
function GetADUserInfo(username){
	function sam2distinguished(samname) {
		var ADS_NAME_INITTYPE_GC = 3;
		var ADS_NAME_TYPE_1779 = 1;
		var ADS_NAME_TYPE_NT4  = 3;

		try {
			with (new ActiveXObject("NameTranslate")) {
				Init (ADS_NAME_INITTYPE_GC, "");
				Set (ADS_NAME_TYPE_NT4, samname);
				return Get(ADS_NAME_TYPE_1779);
			}
		} catch(e) {}
	}

	if (username) {
		var LDAPstring = sam2distinguished(username);
		if (LDAPstring) return GetObject('LDAP://' + LDAPstring);
	}
	return {};
}

// Подключение к удаленному компьютеру
function getRemoteComp(pcname){
	try {
		var oSWbemLocator = new ActiveXObject("WbemScripting.SWbemLocator");
		return {
			CIM: oSWbemLocator.ConnectServer(pcname, "root\\CIMV2"),
			Reg: oSWbemLocator.ConnectServer(pcname, "root\\DEFAULT").Get("StdRegProv")
		};
	} catch(e) {
		var error = e.number & 0xFFFF;
		if (error == 5) {
			var oArguments = {};
			oArguments.compname = pcname;
			oArguments.funcCookie = Cookie;
			var ret = showModalDialog('code/logon.html', oArguments, 'resizable:no; scroll:no; status:no; help:no; dialogWidth:422px; dialogHeight:252px;');
			if (ret) {
				if (ret.error) {
					oWshShell.Popup(ret.error, 2, "Connect with remembed credentials", 16);
				} else {
					return ret;
				}
			}
		} else {
			oWshShell.Popup("Error connecting to host " + pcname + "\n" + error + " : " + e.description, 2, "Connect with current credentials", 16);
		}
	}
}

function NameOrIp(i) {
	if (oMainSet.priority_ip) return (aHosts[i].ip || aHosts[i].hostname || aHosts[i].name);
	return (aHosts[i].hostname || aHosts[i].name || aHosts[i].ip);
}

// Return object {ip, name, hostname, domain}
function HostResolve(host, i) {
	var out_file = oFSO.BuildPath(oFSO.GetSpecialFolder(2), oFSO.GetTempName());
	var cmd = 'cmd /c ping -a -n 1 -4 ' + host + ' >' + out_file;
	oWshShell.Run(cmd, 0, true);
	if (!oFSO.FileExists(out_file)) return;
	var out = ReadTextFile(out_file);
	oFSO.DeleteFile(out_file, true);
	if (/(([\w\-]+)\.?([\w\-\.]*)) \[([\d\.\:]+)\]/.test(out)) {
		var r = {};
		r.hostname = RegExp.$1;
		r.name = RegExp.$2;
		r.domain = RegExp.$3;
		r.ip = RegExp.$4;
		if ((!r.hostname) && r.name && r.domain) r.hostname = r.name + '.' + r.domain;
		return r;
	}
}

function arr2str(arr) {
	if (!arr) return '';
	switch(arr.length){
		case 0:  return '';
		case 1:  return arr[0];
		default: return '[' + arr.join('|') + ']';
	}
}

// Чтение информации с удаленного ПК
function GetHostInfo(i){
	var ipname = NameOrIp(i);
	var aHosts_tmp = ObjClone(aHosts[i]);

	var r = HostResolve(ipname, i);
	if (r) {
		if (!IsProtect(i, 'hostname'))             aHosts[i].hostname = r.hostname || '';
		if (!IsProtect(i, 'name'))                 aHosts[i].name = r.name || '';
		if (!IsProtect(i, 'domain'))               aHosts[i].domain = r.domain || '';
		if (!IsProtect(i, 'ip'))                   aHosts[i].ip = r.ip || '';
	}

	if (IsContainArray(['computer', 'server', 'notebook', 'thinclient'], aHosts[i].type)) {

		if (aHosts[i].type != 'thinclient') {
			var oRemoteComp = getRemoteComp(ipname);
			if (oRemoteComp) {
				oWMIService = oRemoteComp.CIM;
				oWMIRegistry = oRemoteComp.Reg;
				r = GetComputerSystem();
				if (!IsProtect(i, 'name'))                 aHosts[i].name = r.name || aHosts[i].name || '';
				if (!IsProtect(i, 'domain'))               aHosts[i].domain = r.domain || aHosts[i].domain || '';
				if (!IsProtect(i, 'username'))             aHosts[i].username = r.username || '';
				if (!IsProtect(i, 'user'))                 aHosts[i].user = r.user || '';
				if (!IsProtect(i, 'SID'))                  aHosts[i].SID = r.SID || '';
				if (!IsProtect(i, 'UserProfile'))          aHosts[i].UserProfile = r.UserProfile || '';

				r = GetNetInfo();
				if (!IsProtect(i, 'mac'))                  aHosts[i].mac = r.MACAddress || '';
				if (!IsProtect(i, 'ip_all'))               aHosts[i].ip_all = arr2str(r.IPAddress);
				if (!IsProtect(i, 'IPSubnet'))             aHosts[i].IPSubnet = arr2str(r.IPSubnet);
				if (!IsProtect(i, 'DefaultIPGateway'))     aHosts[i].DefaultIPGateway = arr2str(r.DefaultIPGateway);
				if (!IsProtect(i, 'DNSServerSearchOrder')) aHosts[i].DNSServerSearchOrder = arr2str(r.DNSServerSearchOrder);

				if (!IsProtect(i, 'PCSerialNumber'))       aHosts[i].PCSerialNumber = GetPCSerialNumber() || '';
				if (!IsProtect(i, 'OSVersion'))            aHosts[i].OSVersion = GetOperatingSystem() || '';

				r = GetProcessorInfo();
				if (!IsProtect(i, 'proc_name'))            aHosts[i].proc_name = r.Name || '';
				if (!IsProtect(i, 'proc_clock'))           aHosts[i].proc_clock = r.CurrentClockSpeed || '';

				r = GetMemoryInfo();
				if (!IsProtect(i, 'memory_size'))          aHosts[i].memory_size = r.MemorySize || '';
				if (!IsProtect(i, 'memory_info'))          aHosts[i].memory_info = r.MemoryInfo || '';

				if (!IsProtect(i, 'disks_size'))           aHosts[i].disks_size = arr2str(GetDisksSize());

				if (!IsProtect(i, 'video'))                aHosts[i].video = arr2str(GetVideoControllers());

				r = GetMonitors();
				if (!IsProtect(i, 'MonitorName'))          aHosts[i].MonitorName = arr2str(r.Names);
				if (!IsProtect(i, 'MonitorSerialNumber'))  aHosts[i].MonitorSerialNumber = arr2str(r.SerialNumbers);

				if (!IsProtect(i, 'Printers'))             aHosts[i].Printers = arr2str(GetPrinters(aHosts[i].SID));

				if (!IsProtect(i, 'MappedDrives'))         aHosts[i].MappedDrives = arr2str(GetMappedDrives(aHosts[i].SID));
			}
		}

		r = GetADUserInfo(aHosts[i].username);
		if (!IsProtect(i, 'user_cn'))               aHosts[i].user_cn = r.cn || ''; // ФИО
		if (!IsProtect(i, 'user_info'))             aHosts[i].user_info = r.info ? (r.info.replace(/^.*#/, '').replace(/\s*(Центр|по П).*$/i, '')) : ''; // отдел
		if (!IsProtect(i, 'user_title'))            aHosts[i].user_title = r.title || ''; // должность
		if (!IsProtect(i, 'user_mail'))             aHosts[i].user_mail = r.mail || ''; // e-mail
		if (!IsProtect(i, 'user_telephoneNumber'))  aHosts[i].user_telephoneNumber = r.telephoneNumber || ''; // корпоративный
		if (!IsProtect(i, 'user_otherTelephone'))   aHosts[i].user_otherTelephone = r.otherTelephone || ''; // городской
		if (!IsProtect(i, 'user_wWWHomePage'))      aHosts[i].user_wWWHomePage = r.wWWHomePage || '';
	}
	ObjCompare(aHosts[i], aHosts_tmp, aHosts);
	FillRow(i);
	pending--;
	if (pending==0) {
		StartTimer();
		UncheckAll();
	}
}
