var oCM = new ContextMenu();

// Зададим значения встроенным свойствам объекта ContextMenu:
oCM.icons = true;
var icons_path = 'images\\';

// Добавим пользовательские свойства к объекту:
oCM.column = -1;  // колонка заголовка таблицы на котором открыто контекстное меню
oCM.row = -1;     // ряд таблицы на котором открыто контекстное меню (-1 - контекстного меню нет)

/* ====================================================================
Показывает контекстное меню из пунктов Items

Объект ContextMenu имеет свойства:
 .icons - контекстное меню содержит иконки
 .checks - контекстное меню содержит чекбоксы
 .visible - меню в данный момент видимо (если .visible = true)
 
Объект ContextMenu имеет события:
 .onChange - при выборе пункта меню
 .onUnload - при закрытии меню
 
Каждый объект Item массива Items имеет свойства:
 .text - текст пункта меню (если .text = '-' то вставляется разделитель)
 .icon - ссылка на иконку 16х16 (или символ, задаваемый строкой вида 'O;normal 14px Wingdings;blue')
 .disabled - пункт меню неактивен (если .disabled = true)
 .checked - пункт меню отмечен чекбоксом (если .checked = true)
 .accent - пункт меню выделяется жирным шрифтом (если .accent = true)
*/

function ContextMenu(Items) {
	this.Show = function () {
		var winContextMenu = window.createPopup();
		with (winContextMenu.document.createStyleSheet()){
			addRule('*','font:8pt MS Shell Dlg;');
			addRule('table','background-color:buttonface; font:menu; border:2px outset; border-collapse:collapse; cursor:default;');
			addRule('td','white-space:nowrap; padding:1px 2px;');
			addRule('.check','width:18px; font:18px Marlett; border-right:1px solid buttonshadow;');
			addRule('img','width:16px; height:16px;');
			addRule('div','border:1px inset; height:2px; overflow:hidden;');
		}
		winContextMenu.document.body.innerHTML = '<table id="idItemList"></table>';

		var Items = this.Items;
		for (var i = 0; i < Items.length; i++) {
			var NewRow = winContextMenu.document.all.idItemList.insertRow();
			if (Items[i].text == '-') {
				with (NewRow.insertCell()){
					colSpan = 1 + (this.checks || 0) + (this.icons || 0);
					innerHTML = '<div>&nbsp;</div>';
				}
			} else {
				NewRow.disabled = Items[i].disabled;
				if (this.checks) {
					with (NewRow.insertCell()) {
						className = 'check';
						if (Items[i].checked) innerHTML = 'a';
					}
				}
				if (this.icons) {
					with (NewRow.insertCell()) {
						if (/(\w);([\w\s]+);(\w+)/.test(Items[i].icon)) {
							innerHTML = '&nbsp;&nbsp;<span style="font:' + RegExp.$2 + ';color:' + RegExp.$3 + (Items[i].disabled ? ';disabled' : '')+'">' + RegExp.$1 + '</span>';
						} else {
							innerHTML = '&nbsp;<img src="' + Items[i].icon + '"' + (Items[i].disabled ? ' style=filter:Xray' : '')+'>';
						}
					}
				}
				with (NewRow.insertCell()) {
					if (Items[i].accent) style.fontWeight = 'bolder';
					innerHTML = '&nbsp;' + Items[i].text + '&nbsp;';
				}
				// Обработка событий:
				NewRow.name = i;
				NewRow.onmouseover = function () {
					this.style.background = "activecaption";
					this.style.color = "highlighttext";
				};
				NewRow.onmouseout = function () {
					this.style.background = "";
					this.style.color = "";
				};
				var _f0 = this.onChange;
				NewRow.onclick = function () {
					_f0 (this.name);
					winContextMenu.hide();
				};
			}
		}

		winContextMenu.show(0, 0, 0, 0, document.body); // показываем меню (только для вычисления реальных размеров)
		var cm_width = winContextMenu.document.body.scrollWidth;
		var cm_height = winContextMenu.document.body.scrollHeight;
		var cm_y = (event.y+cm_height+window.screenTop > screen.availHeight) ? screen.availHeight-cm_height-window.screenTop : event.y;
		this.visible = true;
		winContextMenu.show(event.x, cm_y, cm_width, cm_height, window.document.body); // показываем меню заново, но уже вычисленного размера

		// Обработка события Unload
		var T = this;
		var _f1 = this.onUnload;
		winContextMenu.document.body.onunload = function(){
			T.visible = false;
			if (_f1) _f1();
		};
	}
}

// Контекстное меню заголовка таблицы
function ShowContextMenuHeader(){
	oCM.icons = true;
	oCM.Items = [
		{
			text: 'Add Column',
			icon: icons_path + 'add.ico',
			command: 'ColumnAdd()'
		},
		{
			text: 'Delete Column',
			icon: icons_path + 'delete.ico',
			command: 'ColumnDelete()'
		},
		{
			text: 'Edit Column Contents',
			icon: icons_path + 'edit.ico',
			command: 'ColumnEdit()'
		},
		{
			text: 'Sort by This Column',
			icon: icons_path + 'sort.ico',
			command: 'ColumnSort()'
		}
	];
	oCM.onChange = function(item_num) {
		setTimeout(oCM.Items[item_num].command, 0);
	};
	oCM.onUnload = function() {
		idHeaderFixed.cells[oCM.column*2].className = ""; // Удаляем подсветку с заголовка колонки на котором открывалось контекстное меню
	};
	idHeaderFixed.cells[oCM.column*2].className = "hglight"; // Подсвечиваем заголовок колонки на котором открывается контекстное меню
	oCM.Show();
}

// Контекстное меню из единственного пункта "AddHosts"
function ShowContextMenuAddHosts(){
	oCM.icons = true;
	oCM.Items = [
		{
			text: 'Add Hosts',
			icon: icons_path + 'add.ico',
			command: 'AddHosts()'
		}
	];
	oCM.onChange = function(item_num) {
		setTimeout(oCM.Items[item_num].command, 0);
	};
	oCM.onUnload = null;
	oCM.Show();
}

// Дефолтное контекстное меню
function ShowContextMenuDefault(){
	oCM.icons = true;
	oCM.Items = [
		{
			text: 'Add Hosts',
			icon: icons_path + 'add.ico',
			command: 'AddHosts()'
		},
		{
			text: 'Delete Host',
			icon: icons_path + 'delete.ico',
			command: 'RemoveSelHost()'
		},
		{
			text: 'Get Host Info',
			icon: icons_path + 'info.ico',
			command: 'GetSelHostInfo()',
			disabled: (aHosts[oCM.row].online == -1)
		},
		{
			text: 'Edit Host Info',
			icon: icons_path + 'edit.ico',
			command: 'EditHostInfo()'
		}
	];
	oCM.onChange = function(item_num) {
		setTimeout(oCM.Items[item_num].command, 0);
	};
	oCM.onUnload = function() {
		idHostsList.rows[oCM.row].className = ""; // Удаляем подсветку с ряда на котором открывалось контекстное меню
	};
	idHostsList.rows[oCM.row].className = "hglight"; // Подсвечиваем ряд на котором открывается контекстное меню
	oCM.Show();
}

// Проверка заблокирован ли экран ПК
function IsWindowsLocked(i) {
	if (IsContainArray(['computer', 'server', 'notebook'], aHosts[i].type)) {
		var oRemoteComp = getRemoteComp(NameOrIp(i));
		if (oRemoteComp) {
			var colItems = oRemoteComp.CIM.ExecQuery ("SELECT * FROM Win32_Process WHERE Name = 'LogonUI.exe'");
			if (colItems.Count) {
				var oIcon = document.getElementsByName('idIcon')[i];
				oIcon.style.filter = 'alpha(opacity=50)';
				return;
			}
		}
	}
}

// Контекстное меню ряда таблицы
function ShowContextMenuRow(){
	if (event.ctrlKey && aHosts[oCM.row].online >= 0) {
		setTimeout('IsWindowsLocked("'+oCM.row+'")', 0);
		return;
	}
	var n = 0;
	oCM.Items = [];
	while (true){
		var item = oCMSet['cm_item'+n];
		if (!item) break;
		var oItem = {};
		if ((item == 'separator') || (item == '-')) {
			oItem.text = '-';
		} else {
			switch(oCMSet['cm_offline_disabled'+n]){
				case '1':
					oItem.disabled = (aHosts[oCM.row].online == -1);
					break;
				case '-1':
					oItem.disabled = (aHosts[oCM.row].online >= 0);
					break;
			}
			oItem.text = item;
			oItem.icon = (/\\/.test(oCMSet['cm_icon'+n]) ? '' : icons_path) + oCMSet['cm_icon'+n];
			oItem.command = oCMSet['cm_cmd'+n];
			oItem.accent = (n==oCMSet['cm_default']);
		}
		oCM.Items.push(oItem);
		n++;
	}
	oCM.onChange = function(item_num) {
		ContextMenuRunItem(oCM.Items[item_num].command);
	};
	oCM.onUnload = function() {
		idHostsList.rows[oCM.row].className = ""; // Удаляем подсветку с ряда на котором открывалось контекстное меню
	};
	idHostsList.rows[oCM.row].className = "hglight"; // Подсвечиваем ряд на котором открывается контекстное меню
	oCM.Show();
}

// Запуск команды из пункта контекстного меню
function ContextMenuRunItem(cmd) {
	// Замещает все найденные в тексте <параметры> соответсвующими их значениями (i - номер хоста)
	function ExtractValue(param, i) {
		var err = false;
		function replacer(_sub, S1, S2, _offset, _str) {
			var ret = (S1=='') ? oMainSet[S2] : aHosts[i][S2];
			if (!ret) {
				if ((S1 != '') && (S2 == 'hostname')) {  // если значение <host.hostname> не задано
					if (aHosts[i]['name']) {
						if (aHosts[i]['domain']) {
							ret = aHosts[i]['name'] + '.' + aHosts[i]['domain'];
						} else {
							ret = aHosts[i]['name'];
						}
					}
				}
				if ((S1 != '') && (S2 == 'name')) ret = aHosts[i]['ip']; // если значение <host.name> не задано, используем вместо него значение <host.ip>
				if (typeof(ret)=='undefined') {
					if (oMainSet.true_udefined) {
						return '';
					} else {
						alert ('Parameter\n'+ ((S1=='') ? '[main' : '[host'+i) + '] ' + S2 + '\nnot defined!');
						err = true;
					}
				}
			}
			return ret;
		}
		var re = /<([^.]*?)[.]?([^.>]*?)>/g;
		var value = param.replace(re, replacer);
		return err ? '' : value;
	}

	if (/^@([\w]+) *\(?([^)]*)\)?/.test(cmd)) {
		// Internal command
		var func = RegExp.$1;
		var param = RegExp.$2.replace(/\\/g, '\\\\');
		if (param) {
			setTimeout (func + '(' + param + ')', 0);
		} else {
			setTimeout (func + '(' + oCM.row + ')', 0);
		}
	} else {
		// External command
		var sel_hosts = GetSelectedHosts();
		if (!sel_hosts) return;
		var host_num, command;
		StopTimer();
		for (var i = 0; i < sel_hosts.length; i++) {
			host_num = sel_hosts[i];
			command = ExtractValue(cmd, host_num);
			if (command) {
				arrRunCmd.push({
					host: host_num,
					cmd: command
				});
			}
		}
		setTimeout ('RunExternalCommand()', 0);
	}
}

var arrRunCmd = [];

// Запуск внешней команды
function RunExternalCommand() {
	var runcmd = arrRunCmd.shift();
	var host_num = runcmd.host;
	var command = oWshShell.ExpandEnvironmentStrings(runcmd.cmd);
	oWshShell.CurrentDirectory = script_path;
	if (/^(.+)\s*\{host\.(\w+)\}$/.test(command)) {
		var tmp = oWshShell.ExpandEnvironmentStrings('%temp%\\~out.tmp');
		command = '%comspec% /c ' + RegExp.$1 + ' >"' + tmp + '"';
		param = RegExp.$2;
		oWshShell.Run(command, 0, true);
		var out = ReadTextFile(tmp);
		if (out) {
			out = out.trim();
			if ((out != aHosts[host_num][param]) && (!IsProtect(host_num, param))) {
				aHosts[host_num][param] = out;
				FillRow(host_num);
				SaveButtonEnable(aHosts);
			}
		}
	} else {
		try {
			// command = prompt(script_path, command); if (!command) return;
			oWshShell.Run(command);
		} catch(e) {
			if (oWshShell.Popup("Don\'t run command:\n" + command + "\n\nEdit context menu now?", 5, app_name, 52) == 6) {
				EditContextMenu();
			}
		}
	}
	document.getElementsByName("idMarkHost")[host_num].checked = false;
	if (arrRunCmd.length) {
		setTimeout ('RunExternalCommand()', 0);
	} else {
		StartTimer();
		idCheckAll.checked = false;
	}
}