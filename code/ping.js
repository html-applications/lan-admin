// Проверка доступности хоста

var pending = 0; // счетчик еще не выполненных запросов

// Посылка ping-запроса на хост номер i
function Ping(i) {
	var objWMIService = GetObject("winmgmts:\\\\.\\Root\\CIMV2");
	var objContext = new ActiveXObject("WbemScripting.SWbemNamedValueSet");
	objContext.Add ("index", i);
	objWMIService.ExecQueryAsync(idPing, "select * from Win32_PingStatus where address ='" + NameOrIp(i) + "'", null, null, null, objContext);
}

// Обработка ответа на ping
function fOnObjectReady(objWbemObject, objWbemAsyncContext){
	var i = objWbemAsyncContext("index");
	aHosts[i].online = (objWbemObject.StatusCode == 0) ? objWbemObject.ResponseTime : -1;
	if (objWbemObject.ProtocolAddress && (/\d+\.\d+\.\d+\.\d+/.test(objWbemObject.ProtocolAddress))) {
		HostIpUpdate(i, objWbemObject.ProtocolAddress);
	}
	RowEnabled(i);
	pending--;
	if (pending==0) StartTimer();
}

// "Ping" на определенный порт хоста
// Используется утилита tcping.exe <http://www.elifulkerson.com/projects/tcping.php>)
function PingPort(i) {
	var out_file = oFSO.BuildPath(oFSO.GetSpecialFolder(2), oFSO.GetTempName());
	var cmd = 'cmd /c "' + util_dir + '\\tcping.exe" -n 1 ' + NameOrIp(i) + ' ' + aHosts[i].port_check + ' >' + out_file;
	oWshShell.Run(cmd, 0, true);
	if (oFSO.FileExists(out_file)) {
		var out = ReadTextFile(out_file);
		try {oFSO.DeleteFile(out_file, true)} catch(e) {}
		aHosts[i].online = (/Probing ([\d.]+):[^№]+Average = (\d+)\.*\d*ms/.test(out)) ? RegExp.$2 : -1;
		HostIpUpdate(i, RegExp.$1);
	}
	RowEnabled(i);
	pending--;
	if (pending==0) StartTimer();
}

// Обновляет параметр ip хоста i
function HostIpUpdate(i, ip) {
	if ((/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(ip)) && (!IsProtect(i, 'ip')) && (aHosts[i].ip != ip)) {
		HHCtrl.TextPopup('Update IP Address on host ' + (aHosts[i].hostname || aHosts[i].host) + '\n' + (aHosts[i].ip || '') + ' -> ' + ip, "MS Sans Serif, 8, , plain", 4, 4, -1, -1);
		aHosts[i].ip = ip;
		FillRow(i);
		SaveButtonEnable(aHosts);
	}
}

// Пинг на очередной хост из arrPingHosts
function PingNextHost(){
	if (arrPingHosts.length > 0) {
		var i = arrPingHosts.shift();
		pending++;
		if (aHosts[i].port_check) {
			PingPort(i);
		} else {
			Ping(i);
		}
		setTimeout('PingNextHost()', 0);
	}
}

var arrPingHosts = []; // содержит номера пингуемых хостов

// Создает массив пингуемых хостов
function CreatePingArr() {
	if (aHosts.length < 1) return;
	StopTimer();
	idStatus.innerText = "Ping all hosts...";
	arrPingHosts = [];
	for (var i=0; i<aHosts.length; i++) {
		arrPingHosts.push(i);
	}
	setTimeout('PingNextHost()', 0);
}

var timerID = null; // Идентификатор таймера

function StopTimer() {
	if (timerID) {
		clearInterval(timerID);
		idTimer.value = 0;
		timerID = null;
	}
}

function StartTimer() {
	StopTimer();
	idTimer.value = oMainSet.ping_interval;
	idStatus.innerText = "Ready";
	timerID = setInterval(Timer, 1000);
}

function Timer() {
	if (idTimer.value==0) {
		if (CheckINI()) return;
		CreatePingArr();
	} else {
		idTimer.value--;
	}
}
