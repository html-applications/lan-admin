// Изменение ширины колонок таблицы

var startX = 0;
var startWidth = 0;
var cell_idx = 0;

// Старт изменения размера колонки (начали двигать зажатую мышь)
function ColumnResize_start() {
	startX = event.clientX;
	cell_idx = event.srcElement.cellIndex - 1;
	startWidth = idHeaderFixed.cells[cell_idx].offsetWidth;
	idHeaderFixed.cells[cell_idx].style.background = 'infobackground';
	document.attachEvent('onmousemove', ColumnResize_move);
	document.attachEvent('onmouseup', ColumnResize_end);
}

// Изменение размера колонки (двигаем зажатую мышь)
function ColumnResize_move(){
	if (event.button <= 1) {
		var column_width = startWidth + event.clientX - startX;
		if (column_width > 10) idHeaderFixed.cells[cell_idx].style.width = column_width;
	}
}

// Конец изменения размера колонки (отпустили мышь)
function ColumnResize_end() {
	idHeaderFixed.cells[cell_idx].style.background = '';
	document.detachEvent('onmousemove', ColumnResize_move);
	document.detachEvent('onmouseup', ColumnResize_end);
	var column_width_old = aColumnsWidth[cell_idx/2];
	if (event.button <= 1) {
		aColumnsWidth[cell_idx/2] = idHeaderFixed.cells[cell_idx].offsetWidth;
	} else { // если нажата правая кнопка - сбрасываем ширину в auto
		aColumnsWidth[cell_idx/2] = '';
		idHeaderFixed.cells[cell_idx].style.width = '';
	}
	if (aColumnsWidth[cell_idx/2] != column_width_old) {
		oMainSet.column_width = aColumnsWidth.join(',');
		Cookie('column_width', oMainSet.column_width);
	}
}
