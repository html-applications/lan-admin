// Чтение ini файла в объект со свойствами идентичными именам ключей
function ReadIniFile(ini_file) {
	var oINI = {};
	var text = ReadTextFile(ini_file);
	if (text) {
		var arr_lines = text.match(/[^\r\n]+/g);
		var section, param, value;
		for(var i = 0, line; line = arr_lines[i++];) {
			line = line.replace(/^\s*/, '').replace(/\s*$/, '');
			if (!/^[;#]/.test(line)) {        // comment
				if (/^\[(.+)\]/.test(line)) { // section
					section = RegExp.$1;
					if ((section) && (!oINI[section])) oINI[section] = {};
				}
				else if (/^(.+?)\s*=\s*(.+)/.test(line)){ // param=value
					param = RegExp.$1;
					value = RegExp.$2;
					oINI[section][param] = value;
				}
			}
		}
	}
	return oINI;
}

// Сохранение объекта INI в ini-файл
function SaveINI(oINI, ini_file){
	function WriteArray(arr){
		for (var i in arr){
			var value = arr[i];
			if (typeof(value)=='object') {
				file.Write('[' + i + ']\r\n');
				WriteArray(value);
			}else{
				value = String(value);
				if ((value != '') && (value != 'undefined')) {
					file.Write(i + '=' + value + '\r\n');
				}
			}
		}
	}
	var D = new Date();
	var ini_back = ini_file.replace(/\.ini$/i, D.getFullYear()+("0"+(D.getMonth()+1)).slice(-2)+("0"+(D.getDate())).slice(-2)+("0"+(D.getHours())).slice(-2)+("0"+(D.getMinutes())).slice(-2)+".bak");
	oFSO.CopyFile(ini_file, ini_back, true);
	var file = oFSO.OpenTextFile(ini_file, 2, true);
	file.WriteLine('; Saved by ' + oWshShell.ExpandEnvironmentStrings('%USERNAME%'));
	WriteArray(oINI);
	file.Close();
}

// Сохранение, восстановление, удаление настроек из cookie
function Cookie(name, value){
	function SetExpires(t) {
		var cookie_date = new Date();
		cookie_date.setTime(cookie_date.getTime() + t);
		return cookie_date.toGMTString();
	}
	name = profile + '_' + name;
	if (value === undefined) {
		// Get Cookie
		if ((new RegExp('(?:^|; )'+name+'=([^;]*)')).test(document.cookie)) return unescape(RegExp.$1);
	} else {
		if (value === null) {
			// Delete Cookie
			document.cookie = name += "=; expires=" + SetExpires(-1);
		} else {
			// Set Cookie
			document.cookie = name + "=" + escape(value) + "; expires=" + SetExpires(1000*60*60*24*365);
		}
	}
}

function GetDateLastModified(fname) {
	return oFSO.GetFile(fname).DateLastModified + '';
}

// Сохранение всех настроек
function SaveSettings(){
	var oTmp = {};
	var ini_file = '';
	if (oMainSet.must_save) {
		delete oMainSet.must_save;
		oTmp.main = oMainSet;
		ini_file = profile_path + '\\main.ini';
		SaveINI(oTmp, ini_file);
		mainIniDateLastModified = GetDateLastModified(ini_file);
	}
	if (oCMSet.must_save) {
		delete oCMSet.must_save;
		oTmp = {}; oTmp.context_menu = oCMSet;
		ini_file = profile_path + '\\menu.ini';
		SaveINI(oTmp, ini_file);
		menuIniDateLastModified = GetDateLastModified(ini_file);
	}
	if (aHosts.must_save) {
		delete aHosts.must_save;
		oTmp = {}; 
		for (var i = 0; i < aHosts.length; i++) oTmp['host'+i] = aHosts[i];
		ini_file = profile_path + '\\hosts.ini';
		SaveINI(oTmp, ini_file);
		hostsIniDateLastModified = GetDateLastModified(ini_file);
	}
	oWshShell.Popup("All settings successfully saved to profile " + profile, 1, document.title, 64);
	SaveButtonEnable();
}

// Проверка модифицирован ли INI файл другой программой
function CheckINI() {
	if (oFSO.FileExists(profile_path + '\\main.ini') &&
		oFSO.FileExists(profile_path + '\\menu.ini') &&
		oFSO.FileExists(profile_path + '\\hosts.ini')) {
		var real_mainIniDateLastModified  = GetDateLastModified(profile_path + '\\main.ini');
		var real_menuIniDateLastModified  = GetDateLastModified(profile_path + '\\menu.ini');
		var real_hostsIniDateLastModified = GetDateLastModified(profile_path + '\\hosts.ini');
		if ((real_mainIniDateLastModified  != mainIniDateLastModified) ||
			(real_menuIniDateLastModified  != menuIniDateLastModified) ||
			(real_hostsIniDateLastModified != hostsIniDateLastModified)) {
			StopTimer();
			if (oWshShell.Popup('INI-file changed by another program.\n Reload with the new data?', 0, document.title, 36) == 6) {
				idSave.disabled = true;
				isReload = true;
				window.location.reload(true);
			} else {
				mainIniDateLastModified = real_mainIniDateLastModified;
				menuIniDateLastModified = real_menuIniDateLastModified;
				hostsIniDateLastModified = real_hostsIniDateLastModified;
				StartTimer();
			}
			return true;
		}
	}
}

// Активация/деактивация кнопки Save. obj - объект [oMainSet|oCMSet|aHosts] в который внесены изменения
function SaveButtonEnable(obj) {
	if (obj) {
		obj.must_save = true;
		if (idSave.disabled) {
			idSave.style.filter = '';
			idSave.style.cursor = 'pointer';
			idSave.disabled = false;
		}
	} else {
		idSave.style.filter = 'progid:DXImageTransform.Microsoft.Emboss() progid:DXImageTransform.Microsoft.Alpha(opacity=50)';
		idSave.style.cursor = '';
		idSave.disabled = true;
	}
}

// Сравнение двух объектов obj1 и obj2 .Если отличны, то активируется кнопка Save
// objS - объект [oMainSet|oCMSet|aHosts] в который внесены изменения
function ObjCompare(obj1, obj2, objS) {
	function PropCompare(obj1, obj2, prop) {
		if (prop != 'online') {
			if ((obj1[prop] || '') != (obj2[prop] || '')) SaveButtonEnable(objS);
		}
	}
	if (!objS.must_save) for (var prop in obj1) PropCompare(obj1, obj2, prop);
	if (!objS.must_save) for (var prop in obj2) if (!obj1[prop]) PropCompare(obj1, obj2, prop);
}

// Преобразование объекта, извлекаемого из hosts.ini в массив aHosts (попутно заполняем arrProps)
function GetArrHosts() {
	var oTmp = ReadIniFile(profile_path + '\\hosts.ini');
	for (var h in oTmp) {
		aHosts[aHosts.length] = oTmp[h];
		for (var param in oTmp[h]) if (!IsContainArray(arrProps, param)) arrProps[arrProps.length] = param;
	}
}

var profile = LanAdm.commandLine.replace(/^".*?" */,'');
if ((profile=="") || (!oFSO.FolderExists(script_path + 'profiles\\' + profile))) {
	showModalDialog('code/profile.html', self, 'status:no; help:no; dialogWidth:10px; dialogHeight:30px; dialogTop:'+screenTop+'; dialogLeft:'+screenLeft);
	if (profile=="") self.close();
}
document.title = profile + ' - ' + app_name + ' v.' + LanAdm.version;

var profile_path = script_path + 'profiles\\' + profile;
var mainIniDateLastModified  = GetDateLastModified(profile_path + '\\main.ini');
var menuIniDateLastModified  = GetDateLastModified(profile_path + '\\menu.ini');
var hostsIniDateLastModified = GetDateLastModified(profile_path + '\\hosts.ini');
var oMainSet = ReadIniFile(profile_path + '\\main.ini').main || {};
var oCMSet = ReadIniFile(profile_path + '\\menu.ini').context_menu || {};
var aHosts = [];
var arrProps = []; // массив с уникальными именами всех используемых host-параметров
GetArrHosts();
var isReload = false; // признак перезагрузки приложения (в противном случае подразумевается завершение работы)

var win_size = Cookie('win_size');
if (win_size) {
	win_size = win_size.split(',');
	oMainSet.width = win_size[0];
	oMainSet.height = win_size[1];
} else {
	if (!oMainSet.width)  oMainSet.width = 600;
	if (!oMainSet.height) oMainSet.height = 400;
	if (oMainSet.width  > window.screen.width-100) oMainSet.width  = window.screen.width-100;
	if (oMainSet.height > window.screen.height-60) oMainSet.height = window.screen.height-60;
}
window.resizeTo(oMainSet.width, oMainSet.height);

var columns_width = Cookie('column_width') || oMainSet.column_width;
var aColumnsWidth = columns_width ? columns_width.split(',') : [];

// Default value
if (!oMainSet.column_headers)        oMainSet.column_headers = 'Description,IP,Computer Name,Current User,OS';
if (!oMainSet.column_contents)       oMainSet.column_contents = '<host.description>,<host.ip>,<host.hostname>,<host.username>,<host.OSVersion>';

if (!oMainSet.response_time_max)     oMainSet.response_time_max = 6;
if (!oMainSet.ping_interval)         oMainSet.ping_interval = 60;

var aColumnsHeader  = oMainSet.column_headers.split(',');
var aColumnsContent = oMainSet.column_contents.split(',');

oWshShell.CurrentDirectory = script_path;
var util_dir = script_path + 'utils';
