// Инициализация частоупотребимых объектов ActiveX
var oWshShell = new ActiveXObject("WScript.Shell");
var oFSO = new ActiveXObject("Scripting.FileSystemObject");

// Клонирование объекта
function ObjClone(obj) {
	var tmp = {};
	for (var prop in obj) tmp[prop] = obj[prop];
	return tmp;
}

// Удаляет все дочерние элементы узла DOM
function RemoveChildren(node) {
	while (node.firstChild) node.removeChild(node.firstChild);
}

// Содержится элемент в массиве или нет?
function IsContainArray(Arr, elem){
	return (typeof(GetElementIndex(Arr, elem)) == 'number')
}

// Возвращает индекс элемента в массиве
function GetElementIndex(Arr, elem){
	var i = 0;
	while(Arr[i]){
		if (Arr[i] == elem) return i;
		i++;
	}
}

// Преобразует HTML стринг в текст
function Html2text(html) {
	var tmp = document.createElement("DIV");
	tmp.innerHTML = html;
	var ret = tmp.innerText;
	delete tmp;
	return ret;
}

// Чтение текстового файла
function ReadTextFile(filename) {
	if ((oFSO.FileExists(filename)) && (oFSO.GetFile(filename).Size > 0)) {
		with (oFSO.OpenTextFile(filename)) {
			var text = ReadAll();
			Close();
		}
	}
	return text || '';
}

// Обработчик ошибок скрипта
function ErrorHandler (msg, url, line){
	var version = navigator.appVersion;
	try {
		var arr = /.*(MSIE.*?);.*(Windows.*?);.*/.exec(version);
		version = arr[1] + '; ' + arr[2];
	} catch(e) {}
	var text = (typeof(window.dialogArguments)=="undefined") ? (app_name + ' v.' + LanAdm.version) : (window.dialogArguments.app_name + ' v.' + window.dialogArguments.LanAdm.version);
	text += ' programm error:\n\n';
	text += 'VERSION:  ' + version + '\n';
	text += 'DOCUMENT:  ' + unescape(document.URL).replace(/.*\\/,'') + '\n';
	text += 'SCRIPT:  ' + url.replace(/.*[\\/]/,'') + '\n';
	text += 'ERROR:  ' + msg + '\n';
	text += 'LINE:  ' + line + '\n';
	window.clipboardData.setData("Text", text);
	text += '\nThe error text is copied to clipboard!';
	alert(text);
	window.close(true);
	return true;
}

// Замена <IMG src="data:image/gif;base64, на обычную ссылку на файл в %temp% (нужно только для WinXP)
function base64decode(img){
	var oXMLDoc = new ActiveXObject("MSXml2.DOMDocument");
	var eTmp = oXMLDoc.createElement("tmp");
	eTmp.dataType = "bin.base64";
	eTmp.text = img.src.replace(/^.+?base64,/, '');
	var writeBytes = eTmp.nodeTypedValue;

	var tmpfile = oFSO.BuildPath(oFSO.GetSpecialFolder(2), oFSO.GetTempName());
	var oStream = new ActiveXObject("SAPI.spFileStream");
	oStream.Open(tmpfile, 2);
	oStream.Write(writeBytes);
	oStream.Close();
	img.src = tmpfile;
}

// Установка размера диалогового окна (по размеру содержимого)
function SetDialogSize(){
	function px2number(size_px){
		return Number(size_px.replace(/\D*/g,''));
	}
	var dialog_height_max = 600; // если высота таблицы больше этого значения - добавляем scrollbar
	var scrollbar_width = 17;    // ширина полосы прокрутки
	var incW = incH = 0;

	if (self.dialogWidth != document.body.offsetWidth + "px") {
		// Корректировка размера диалогового окна (т.к. в старых версиях IE эти параметры задают размер диалогового окна целиком, а не размер клиентской области)
		incW = px2number(self.dialogWidth) - document.body.offsetWidth;
		incH = px2number(self.dialogHeight) - document.body.offsetHeight + 3;
	}

	if (document.body.scrollHeight > dialog_height_max){
		// scrollbar
		self.dialogWidth  = document.body.scrollWidth + incW + scrollbar_width + "px";
		self.dialogHeight = dialog_height_max + incH + "px";
	} else {
		// no scrollbar
		self.dialogWidth  = document.body.scrollWidth + incW + "px";
		self.dialogHeight = document.body.scrollHeight + incH + "px";
	}
}

// Чтение реестра
function GetRegistry(oWMIReg, method, hive, key, value){
	var readMethod = oWMIReg.Methods_.Item(method);
	var inputParams = readMethod.InParameters.SpawnInstance_();
	switch(hive){
		case "HKCR": inputParams.hDefKey = 0x80000000; break;
		case "HKCU": inputParams.hDefKey = 0x80000001; break;
		case "HKLM": inputParams.hDefKey = 0x80000002; break;
		case "HKU":  inputParams.hDefKey = 0x80000003; break;
		case "HKCC": inputParams.hDefKey = 0x80000005;
	}
	inputParams.sSubKeyName = key;
	if (value) inputParams.sValueName = value;
	var outputParams = oWMIReg.ExecMethod_(readMethod.Name, inputParams);
	if (outputParams.ReturnValue != 0) return;
	switch(method){
		case "GetStringValue":
		case "GetExpandedStringValue":
			return outputParams.sValue;
		case "GetDWORDValue":
			return outputParams.uValue;
		case "GetMultiStringValue":
			return VBArray(outputParams.sValue).toArray();
		case "GetBinaryValue":
			return VBArray(outputParams.uValue).toArray();
		case "EnumKey":
		case "EnumValues":
			return (outputParams.sNames != null) ? outputParams.sNames.toArray() : null;
	}
}

// Установка атрибута ReadOnly на папку скрипта (чтобы был виден значок, заданный в desktop.ini)
function SetScriptFolderReadOnly(){
	var oFolder = oFSO.GetFolder(script_path);
	var Attrs = oFolder.Attributes;
	if (!(Attrs & 1)) oFolder.Attributes = Attrs ^ 1;
}

// Определяет защищен ли параметр от изменений
function IsProtect(i, prop_name) {
	var p = aHosts[i].protect;
	if (p) return (new RegExp('\\b'+prop_name+'\\b')).test(p);
}

// Аналог VB функции Trim$
String.prototype.trim = function() {return this.replace(/^\s+|\s+$/g, '')};
