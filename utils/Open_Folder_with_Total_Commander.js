// Open Folder with Total Commander

function GetTotalcmdExe (){
	var objWMIService = GetObject("winmgmts:\\\\.\\root\\CIMV2");
	var colProcessList = objWMIService.ExecQuery ("SELECT * FROM Win32_Process WHERE NAME LIKE 'TOTALCMD%.EXE'");
	var enumItems = new Enumerator(colProcessList);
	for (; !enumItems.atEnd(); enumItems.moveNext()){
		return enumItems.item().ExecutablePath;
	}
}

var WshShell = new ActiveXObject("WScript.Shell");
var title = WScript.ScriptName.replace(/_/g, ' ').replace(/\.js$/i, '');
if (WScript.Arguments.length > 0) {
	var cmd_path = WScript.Arguments(0);
	var FSO = new ActiveXObject("Scripting.FileSystemObject");
	if (FSO.FolderExists(cmd_path)) {
		var totalcmd = GetTotalcmdExe();
		if (totalcmd) {
			WshShell.Run('"'+ totalcmd + '" /O /R="' + cmd_path + '"');
		} else {
			WshShell.Popup('Total Commander is not running!', 5, title, 48);
		}
	} else {
		WshShell.Popup('Folder "' + cmd_path + '" not exist!', 5, title, 48);
	}
} else {
	WshShell.Popup('Use:\n' + WScript.ScriptName + ' <path>', 5, title, 64);
}
