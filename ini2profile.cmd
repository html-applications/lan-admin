@echo off
title %~n0
pushd "%~dp0"
echo LAN Administrator :: INI to Profile converter
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo INI files avaliable:
dir /b *.ini /o:n
echo.
for /f "tokens=*" %%F in ('dir /b *.ini /o:-n') do set I=%%F

set /p ini0=INI file name [%I%]:
if not "%ini0%"=="" set I=%ini0%

if not exist "%I%" (
	echo Filename "%I%" not exist!
	exit /b 1
)

set P=%I:.ini=%

set /p profile=Profile name [%P%]:
if not "%profile%"=="" set P=%profile%

if not exist profiles md profiles
:check
if exist "profiles\%P%" (
	set /p P=Profile "%P%" alredy exist! Please enter other name:
	goto :check
)

md "profiles\%P%"

setlocal enabledelayedexpansion
FOR /F "usebackq tokens=*" %%l IN ("%I%") DO (
	if "%%l"=="[main]" set out=main.ini
	if "%%l"=="[context_menu]" set out=menu.ini
	if "%%l"=="[host0]" set out=hosts.ini
	echo.%%l>>"profiles\%P%\!out!"
	<nul set /p "x=*"
)
setlocal disabledelayedexpansion

echo.
echo Convert file "%I%" to profile "%P%" successfully completed!

set /p y=Delete file "%I%"? [Y]:
if not "%y%"=="" (
	if not "%y%"=="Y" (
		if not "%y%"=="y" exit /b
	)
)
del /f "%I%"
