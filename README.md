---
### ![goup.png](http://html-applications.bitbucket.io/images/goup.png) [Return to HTML Application page](http://html-applications.bitbucket.io) ###
---
## ![lan-admin.png](http://html-applications.bitbucket.io/images/lan-admin.png)  [LAN Administrator](http://html-applications.bitbucket.io/lan-admin/readme.html) ##
Is a tool for monitoring, information gathering and launch applications on a hosts on the LAN. This is a hybrid of a database with any data and an application to perform any given commands based on these data.

Мониторинг, сбор информации и запуск сетевых приложений на хостах локальной сети. Это - гибрид базы с произвольными данными и приложения, выполняющего любые заданные команды на основе этих данных.
